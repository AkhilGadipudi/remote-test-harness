﻿/////////////////////////////////////////////////////////////////////
// TestHarness.cs - TestHarness Engine: creates child domains      //
// Author : Akhil Gadipudi       SU-mail : agadipud                //
// Source : Jim Fawcett, CSE681 - Software Modeling and Analysis   //
/////////////////////////////////////////////////////////////////////
/*
 * Package Operations:
 * -------------------
 * TestHarness package provides integration testing services.  It:
 * - receives structured test requests
 * - retrieves files from a repository
 * - executes tests on all code that implements an ITest interface,
 * - reports pass or fail status for each test in a test request
 * - stores test logs in the repository
 * 
 * It contains classes:
 * - TestHarness that runs all tests in child AppDomains
 * - Test and RequestInfo to support transferring test information
 *   from TestHarness to child AppDomain
 * 
 * Required Files:
 * ---------------
 * - TestHarness.cs, BlockingQueue.cs
 * - Interfaces.cs
 * - LoadAndTest, Logger, Messages
 * - CommSerives.cs
 *
 * Maintanence History:
 * --------------------
 * ver 1.0 : 27 Nov 2016
 * - first release
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Security.Policy;    // defines evidence needed for AppDomain construction
using System.Runtime.Remoting;   // provides remote communication between AppDomains
using System.Xml;
using System.Xml.Linq;
using System.Threading;

namespace TestHarness
{
 
  ///////////////////////////////////////////////////////////////////
  // Test and RequestInfo are used to pass test request information
  // to child AppDomain
  //
  [Serializable]
  class Test : ITestInfo
  {
    public string testName { get; set; }
    public List<string> files { get; set; } = new List<string>();
  }
  [Serializable]
  class RequestInfo : IRequestInfo
  {
     public string tempDirName { get; set; }
     public List<ITestInfo> requestInfo { get; set; } = new List<ITestInfo>();
  }
  ///////////////////////////////////////////////////////////////////
  // class TestHarness

  public class TestHarness
  {
    public Comm<TestHarness> comm  = new Comm<TestHarness>();
    public string TH_endPoint  = Comm<TestHarness>.makeEndPoint("http://localhost", 8081);
    string Client_EndPoint = Comm<TestHarness>.makeEndPoint("http://localhost", 8080);
    string Repo_EndPoint = Comm<TestHarness>.makeEndPoint("http://localhost", 8082);
    private Thread rcvThread = null;
    
    private string repoPath_ = "../../../Repository/RepositoryStorage/";
    private string filePath_;
    object sync_ = new object();
    List<Thread> threads_ = new List<Thread>();
    Dictionary<int, string> TLS = new Dictionary<int, string>();

    public TestHarness()
    {
      Console.Write("\n  TestHarness instanciated");
      comm.rcvr.CreateRecvChannel(TH_endPoint);
      rcvThread = comm.rcvr.start(rcvThreadProc);
    }

    void rcvThreadProc()
    {
        while (true)
        {
            Message msg = comm.rcvr.GetMessage();
            if (msg.body == "quit")
                break;
            else
            {
                if (msg.from == Client_EndPoint)
                {
                    Console.Write("\n  TestHarness received a testRequest - Requirment 2");
                    msg.show();
                    processMessages(msg);
                }
                else if (msg.from == Repo_EndPoint)
                {
                        Console.Write("\n  TestHarness received file from repository");
                }
            }
        }
    }


    //----< make path name from author and time >--------------------

    string makeKey(string author)
    {
      DateTime now = DateTime.Now;
      string nowDateStr = now.Date.ToString("d");
      string[] dateParts = nowDateStr.Split('/');
      string key = "";
      foreach (string part in dateParts)
        key += part.Trim() + '_';
      string nowTimeStr = now.TimeOfDay.ToString();
      string[] timeParts = nowTimeStr.Split(':');
      for (int i = 0; i < timeParts.Count() - 1; ++i)
        key += timeParts[i].Trim() + '_';
      key += timeParts[timeParts.Count() - 1];
      key = author + "_" + key;
      return key;
    }
    //----< retrieve test information from testRequest >-------------

    List<ITestInfo> extractTests(Message testRequest)
    {
      Console.Write("\n  Parsing Test Request");
      List<ITestInfo> tests = new List<ITestInfo>();
      XDocument doc = XDocument.Parse(testRequest.body);
      foreach (XElement testElem in doc.Descendants("test"))
      {
        Test test = new Test();
        string testDriverName = testElem.Element("testDriver").Value;
        test.testName = testElem.Attribute("name").Value;
        test.files.Add(testDriverName);
        foreach (XElement lib in testElem.Elements("library"))
        {
          test.files.Add(lib.Value);
        }
        tests.Add(test);
      }
      return tests;
    }
    //----< retrieve test code from testRequest >--------------------

    List<string> extractCode(List<ITestInfo> testInfos)
    {
      Console.Write("\n  Retreving test code from test info");
      List<string> codes = new List<string>();
      foreach (ITestInfo testInfo in testInfos)
        codes.AddRange(testInfo.files);
      return codes;
    }
    //----< create local directory and load from Repository >--------

    RequestInfo processRequestAndLoadFiles(Message testRequest)
    {
      Console.Write("\n  Requesting files from Repository   --  Requirment 6");
      Message msgToRepo = new Message();
      msgToRepo.to = Repo_EndPoint;
      msgToRepo.from = TH_endPoint;
      comm.sndr.PostMessage(msgToRepo);
      
      string localDir_ = "";
      RequestInfo rqi = new RequestInfo();
      rqi.requestInfo = extractTests(testRequest);
      List<string> files = extractCode(rqi.requestInfo);

      localDir_ = makeKey(testRequest.author);            // name of temporary dir to hold test files
      rqi.tempDirName = localDir_;
      lock (sync_)
      {
        filePath_ = System.IO.Path.GetFullPath(localDir_);  // LoadAndTest will use this path
        TLS[Thread.CurrentThread.ManagedThreadId] = filePath_;
      }
      Console.Write("\n  Local Test Directory is created \"" + localDir_ + "\"");
      System.IO.Directory.CreateDirectory(localDir_);

      Console.Write("\n  Loading code into local directory");
      foreach (string file in files)
      {
        string name = System.IO.Path.GetFileName(file);
        string src = System.IO.Path.Combine(repoPath_, file);
        if (System.IO.File.Exists(src))
        {
          string dst = System.IO.Path.Combine(localDir_, name);
          try
          {
            System.IO.File.Copy(src, dst, true);
          }
          catch
          {

          }
          Console.Write("\n    TID" + Thread.CurrentThread.ManagedThreadId + ": received file \"" + name + "\"");
        }
        else
        {
          Console.Write("\n    TID" + Thread.CurrentThread.ManagedThreadId + ": could not received file \"" + name + "\" -- Requirment 3");
        }
      }
      Console.WriteLine();
      return rqi;
    }
    //----< save results and logs in Repository >--------------------

    bool saveResultsAndLogs(ITestResults testResults)
    {
      string logName = testResults.testKey + ".txt";
      System.IO.StreamWriter sr = null;
      try
      {
        sr = new System.IO.StreamWriter(System.IO.Path.Combine(repoPath_, logName));
        sr.WriteLine(logName);
        foreach (ITestResult test in testResults.testResults)
        {
          sr.WriteLine("-----------------------------");
          sr.WriteLine(test.testName);
          sr.WriteLine(test.testResult);
          sr.WriteLine(test.testLog);
        }
        sr.WriteLine("-----------------------------");
      }
      catch
      {
        sr.Close();
        return false;
      }
      sr.Close();
      return true;
    }

    //----< run tests >----------------------------------------------
    ITestResults runTests(Message testRequest)
    {
      AppDomain ad = null;
      ILoadAndTest ldandtst = null;
      RequestInfo rqi = null;
      ITestResults tr = null;

      try
      {
        lock (sync_)
        {
          rqi = processRequestAndLoadFiles(testRequest);
          ad = createChildAppDomain();
          ldandtst = installLoader(ad);

        }
        if (ldandtst != null)
        {
          tr = ldandtst.test(rqi);
        }
        // unloading ChildDomain, and so unloading the library

        saveResultsAndLogs(tr);

        lock (sync_)
        {
          Console.Write("\n  TID" + Thread.CurrentThread.ManagedThreadId + ": unloading: \"" + ad.FriendlyName + "\"  --  Requirment 7\n");
          AppDomain.Unload(ad);
          try
          {
            System.IO.Directory.Delete(rqi.tempDirName, true);
            Console.Write("\n  TID" + Thread.CurrentThread.ManagedThreadId + ": removed directory " + rqi.tempDirName);
          }
          catch (Exception ex)
          {
            Console.Write("\n  TID" + Thread.CurrentThread.ManagedThreadId + ": could not remove directory " + rqi.tempDirName);
            Console.Write("\n  TID" + Thread.CurrentThread.ManagedThreadId + ": " + ex.Message);
          }
        }
        return tr;
      }
      catch(Exception ex)
      {
        Console.Write("\n\n---- {0}\n\n", ex.Message);
        return tr;
      }
    }
    //----< make TestResults Message >-------------------------------

    Message makeTestResultsMessage(ITestResults tr)
    {
      Message trMsg = new Message();
      trMsg.author = "TestHarness";
      trMsg.to = Client_EndPoint;
      trMsg.from = TH_endPoint;
      XDocument doc = new XDocument();
      XElement root = new XElement("testResultsMsg");
      doc.Add(root);
      XElement testKey = new XElement("testKey");
      testKey.Value = tr.testKey;
      root.Add(testKey);
      XElement timeStamp = new XElement("timeStamp");
      timeStamp.Value = tr.dateTime.ToString();
      root.Add(timeStamp);
      XElement testResults = new XElement("testResults");
      root.Add(testResults);
      foreach(ITestResult test in tr.testResults)
      {
        XElement testResult = new XElement("testResult");
        testResults.Add(testResult);
        XElement testName = new XElement("testName");
        testName.Value = test.testName;
        testResult.Add(testName);
        XElement result = new XElement("result");
        result.Value = test.testResult;
        testResult.Add(result);
        XElement log = new XElement("log");
        log.Value = test.testLog;
        testResult.Add(log);
      }
      trMsg.body = doc.ToString();
      return trMsg;
    }
    //----< wait for all threads to finish >-------------------------

    public void wait()
    {
      foreach (Thread t in threads_)
        t.Join();
    }
    //----< main activity of TestHarness >---------------------------

    public void processMessages(Message msg)
    {
      AppDomain main = AppDomain.CurrentDomain;
      HiResTimer hrt = new HiResTimer();
      Console.Write("\n  Starting in AppDomain " + main.FriendlyName + "\n");
      Message testRequest = msg;
      ThreadStart doTests = () => {
        {
          Console.Write("\n  Test request of this client \"{0}\" is handled by TID{1}   --  Requirment 4",msg.author, Thread.CurrentThread.ManagedThreadId);
          hrt.Start();
          ITestResults testResults = runTests(testRequest);
          lock (sync_)
          {
              Message resultMsg = makeTestResultsMessage(testResults);
              Console.Write("\n  Sending Results to Client -- Requirment 6");
              comm.sndr.PostMessage(resultMsg);
              hrt.Stop();
              Console.Write("\n  Time take for processing test request : {0}   -- Requirment 12", hrt.ElapsedMicroseconds);
              Console.Write("\n  Result are sent to repository -- Requiment 8");
          }
        }
      };
        Thread t = new Thread(doTests);
        threads_.Add(t);
        t.Start();
    }

    //----< create child AppDomain >---------------------------------

    public AppDomain createChildAppDomain()
    {
      try
      {
        Console.Write("\n  TID" + Thread.CurrentThread.ManagedThreadId +"  Created child AppDomain - Requirment 5");

        AppDomainSetup domaininfo = new AppDomainSetup();
        domaininfo.ApplicationBase
          = "file:///" + System.Environment.CurrentDirectory;  // defines search path for LoadAndTest library

        //Create evidence for the new AppDomain from evidence of current

        Evidence adevidence = AppDomain.CurrentDomain.Evidence;

        // Create Child AppDomain

        AppDomain ad
          = AppDomain.CreateDomain("ChildDomain", adevidence, domaininfo);

        Console.Write("\n  Created AppDomain");
        return ad;
      }
      catch (Exception except)
      {
        Console.Write("\n  " + except.Message + "\n\n");
      }
      return null;
    }
    //----< Load and Test is responsible for testing >---------------

    ILoadAndTest installLoader(AppDomain ad)
    {
      ad.Load("LoadAndTest");
      //showAssemblies(ad);
      //Console.WriteLine();

      // create proxy for LoadAndTest object in child AppDomain

      ObjectHandle oh
        = ad.CreateInstance("LoadAndTest", "TestHarness.LoadAndTest");
      object ob = oh.Unwrap();    // unwrap creates proxy to ChildDomain
                                  // Console.Write("\n  {0}", ob);

      // set reference to LoadAndTest object in child

      ILoadAndTest landt = (ILoadAndTest)ob;

      // create Callback object in parent domain and pass reference
      // to LoadAndTest object in child

      lock (sync_)
      {
        filePath_ = TLS[Thread.CurrentThread.ManagedThreadId];
        landt.loadPath(filePath_);  // send file path to LoadAndTest
      }
      return landt;
    }

    static void Main(string[] args)
    {
        TestHarness th = new TestHarness();
        th.wait();
        Console.ReadKey();
    }
  }
}
