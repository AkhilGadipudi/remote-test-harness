﻿/////////////////////////////////////////////////////////////////////
// Client.cs - sends TestRequests, displays results, makes queries //
// Author : Akhil Gadipudi       SU-mail : agadipud                //
// Source : Jim Fawcett, CSE681 - Software Modeling and Analysis   //
/////////////////////////////////////////////////////////////////////
/*
 * Package Operations:
 * -------------------
 * Sends Test request to Test Harness and display the result it received from TestHarness
 * Request logs from Repository and displays them
 * 
 * Required Files:
 * - Client.cs, Interfaces.cs, Logger.cs, CommSerives.cs
 * 
 * Maintenance History:
 * --------------------
 * ver 1.0 : 27 Nov 2016
 * - first release
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace TestHarness
{
  public class Client 
  {
    public Comm<Client> comm  = new Comm<Client>();
    public string Client_endPoint = Comm<Client>.makeEndPoint("http://localhost", 8080);
    string TH_EndPoint = Comm<Client>.makeEndPoint("http://localhost", 8081);
    string Repo_EndPoint = Comm<Client>.makeEndPoint("http://localhost", 8082);
    private Thread rcvThread = null;

    public Client()
    {
      Console.Write("\n  Client instantiated");
      comm.rcvr.CreateRecvChannel(Client_endPoint);
      rcvThread = comm.rcvr.start(rcvThreadProc);
    }
    public void sendRepository(Message msg)
    {
        msg.to = Repo_EndPoint;
        comm.sndr.PostMessage(msg);
    }

    public void sendTestHarness(Message msg)
    {
        msg.to = TH_EndPoint;
        comm.sndr.PostMessage(msg);
    }

    void rcvThreadProc()
    {
        while (true)
        {
            Message msg = comm.rcvr.GetMessage();
            if (msg.from == TH_EndPoint)
            {
                Console.Write("\n  Client received message from Test Harness:    -- Requirment 7");
                Console.Write("\n  " + msg.ToString());
                Console.WriteLine();
            }
            if (msg.from == Repo_EndPoint)
            {
                Console.Write("\n\n  Client received logs from Repository:     -- Requirment 9");
                Console.Write("\n  " + msg.body);
                Console.WriteLine();
            }
            if (msg.body == "quit")
            break;
        }
    }

    static void Main(string[] args)
    {
        Console.Write("\n  Implimented using .Net framework and Visual Studio 2015  ---  Requirment 1");
        Client c = new Client();
        Message testreq1 = new Message();
        testreq1.to = c.TH_EndPoint;
        testreq1.from = c.Client_endPoint;
        testreq1.author = "Client1";

        testElement te1 = new testElement("test1");
        te1.addDriver("testdriver.dll");
        te1.addCode("testedcode.dll");
        testElement te2 = new testElement("test2");
        te2.addDriver("td1.dll");
        te2.addCode("tc1.dll");
        testElement te3 = new testElement("test3");
        te3.addDriver("anothertestdriver.dll");
        te3.addCode("anothertestedcode.dll");
        testRequest tr = new testRequest();
        tr.author = "Akhil";
        tr.tests.Add(te1);
        tr.tests.Add(te2);
        tr.tests.Add(te3);
        testreq1.body = tr.ToString();

        Console.Write("\n\n Client1 Sending test request to test harness");
        c.sendTestHarness(testreq1);

        Message testreq2 = testreq1.copy();
        testreq2.author = "Client2";
        Console.Write("\n\n Client2 Sending test request to test harness");
        c.sendTestHarness(testreq2);

        //Request logs from Repository
        Console.Write("\n\n Sending logfile query to Repository");
        Message query = new Message();
        query.from = c.Client_endPoint;
        query.to = c.Repo_EndPoint;
        query.body = "test1";
        c.comm.sndr.PostMessage(query);

        //Close Test Harness
        Message msg2 = new Message();
        msg2.to = c.TH_EndPoint;
        msg2.from = c.Client_endPoint;
        msg2.author = "Akhil";
        msg2.body = "quit";
        c.sendTestHarness(msg2);
        
        //Close Repository
        msg2.to = c.Repo_EndPoint;
        msg2.author = "Akhil";
        msg2.body = "quit";
        c.sendRepository(msg2);


        c.rcvThread.Join();
        Console.ReadKey();
     }
  }
}
