﻿/////////////////////////////////////////////////////////////////////
// Repository.cs - holds test code for TestHarness                 //
// Author : Akhil Gadipudi       SU-mail : agadipud                //
// Source : Jim Fawcett, CSE681 - Software Modeling and Analysis   //
/////////////////////////////////////////////////////////////////////
/*
 * Package Operations:
 * -------------------
 * Receives Log request from and client and return the logs
 * Receives file requests from test harness and streams the file to testharness
 * 
 * Required Files:
 * - Client.cs, Interfaces.cs, Logger.cs, CommSerives.cs
 * 
 * Maintenance History:
 * --------------------
 * ver 1.0 : 27 Nov 2016
 * - first release
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

namespace TestHarness
{ 
  public class Repository
  {
    string repoStoragePath = "..\\..\\..\\Repository\\RepositoryStorage\\";
    public Comm<Repository> comm = new Comm<Repository>();
    public string Repo_endPoint  = Comm<Repository>.makeEndPoint("http://localhost", 8082);
    string TH_EndPoint = Comm<Repository>.makeEndPoint("http://localhost", 8081);
    string Client_EndPoint = Comm<Repository>.makeEndPoint("http://localhost", 8080);
    private Thread rcvThread = null;

    public Repository()
    {
      Console.Write("\n  Creating instance of Repository");
      comm.rcvr.CreateRecvChannel(Repo_endPoint);
      rcvThread = comm.rcvr.start(rcvThreadProc);
    }

    void rcvThreadProc()
    {
        while (true)
        {
            
            Message msg = comm.rcvr.GetMessage();
                if (msg.body == "quit")
                    break;
            if (msg.from == TH_EndPoint)
            {
                    Console.Write("\n  Sending files to TestHarness");
            }
            if (msg.from == Client_EndPoint)
            {
                List<string> queryResult = queryLogs(msg.body);
                if (queryResult.Count == 0)
                {
                    Console.Write("\n   No Logs to send");
                    continue;
                }
                Message msgToClient = makeMessage("Akhil", Repo_endPoint, Client_EndPoint);
                    foreach (string s in queryResult)
                        msgToClient.body += s + "\n";
                comm.sndr.PostMessage(msgToClient);
                Console.Write("\n  Repository sent logs to Client");
            }
            
        }
    }

    public Message makeMessage(string author, string fromEndPoint, string toEndPoint)
    {
      Message msg = new Message();
      msg.author = author;
      msg.from = fromEndPoint;
      msg.to = toEndPoint;
      return msg;
    }
    
    //----< search for text in log files >---------------------------
    public List<string> queryLogs(string queryText)
    {
      List<string> queryResults = new List<string>();
      string path = System.IO.Path.GetFullPath(repoStoragePath);
      string[] files = System.IO.Directory.GetFiles(repoStoragePath, "*.txt");
      foreach(string file in files)
      {
        string contents = File.ReadAllText(file);
        if (contents.Contains(queryText))
        {
          string name = System.IO.Path.GetFileName(file);
          queryResults.Add(name);
        }
      }
      queryResults.Sort();
      queryResults.Reverse();
      return queryResults;
    }

    //----< send files with names on fileList >----------------------
    public bool getFiles(string fileList)
    {
      string path = Path.GetFullPath(@"..\\..\\TestHarness\TestHarnessStorage");
      string[] files = fileList.Split(new char[] { ',' });
      foreach (string file in files)
      {
        string fqSrcFile = repoStoragePath + file;
        string fqDstFile = "";
        try
        {
          fqDstFile = path + "\\" + file;
          File.Copy(fqSrcFile, fqDstFile);
        }
        catch
        {
          Console.Write("\n  could not copy \"" + fqSrcFile + "\" to \"" + fqDstFile);
          return false;
        }
      }
      return true;
    }
    
    static void Main(string[] args)
    {
        Repository repo = new Repository();
        repo.rcvThread.Join();
        Console.ReadKey();
    }
  }
}
