﻿/////////////////////////////////////////////////////////////////////
// ITest.cs - interfaces for communication between system parts    //
// Author : Akhil Gadipudi       SU-mail : agadipud                //
// Source : Jim Fawcett, CSE681 - Software Modeling and Analysis   //
/////////////////////////////////////////////////////////////////////
/*
 * Package Operations:
 * -------------------
 * Interfaces.cs provides interfaces:
 * - IRequestInfo   used by TestHarness
 * - ITestInfo      used by TestHarness
 * - ILoadAndTest   used by TestHarness
 * - ITest          used by LoadAndTest
 *
 * Required files:
 * ---------------
 * - Interfaces.cs
 * 
 * Maintanence History:
 * --------------------
 * ver 1.0 : 27 Nov 2016
 * - first release
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace TestHarness
{

    [ServiceContract]
    public interface ICommunicator
    {
        [OperationContract(IsOneWay = true)]
        void PostMessage(Message msg);

        Message GetMessage();
    }
    
    /////////////////////////////////////////////////////////////
    // used by child AppDomain to invoke test driver's test()

    public interface ITest
  {
    bool test();
    string getLog();
  }

    /////////////////////////////////////////////////////////////
    // used by TestHarness to communicate with child AppDomain

    public interface ILoadAndTest
  {
      ITestResults test(IRequestInfo requestInfo);
      void loadPath(string path);
  }
  public interface ITestInfo
  {
      string testName { get; set; }
      List<string> files { get; set; }
  }
  public interface IRequestInfo
  {
      string tempDirName { get; set; }
      List<ITestInfo> requestInfo { get; set; }
  }
  public interface ITestResult
  {
      string testName { get; set; }
      string testResult { get; set; }
      string testLog { get; set; }
  }
  public interface ITestResults
  {
      string testKey { get; set; }
      DateTime dateTime { get; set; }
      List<ITestResult> testResults { get; set; }
  }
}
